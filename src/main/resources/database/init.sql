/******************** Add Data: User ************************/
insert into users (name , surname , age , email, role) values ('alan','smith',18,'a@ya.ru',1);
insert into users (name , surname , age , email, role) values ('marcus','rashford',23,'b@ya.ru',2);
insert into users (name , surname , age , email, role) values ('daniel','james',21,'c@ya.ru',1);
insert into users (name , surname , age , email, role) values ('dan','greenwood',19,'d@ya.ru',0);
insert into users (name , surname , age , email, role) values ('block','block',19,'ddd@ya.ru',1);

/******************** Add Data: Authenticate ************************/
insert into authenticate (user_id,login, password,Profile_status) values (1,'user', 'user',0);
insert into authenticate (user_id,login, password,Profile_status) values (2,'admin', 'admin',0);
insert into authenticate (user_id,login, password,Profile_status) values (3,'adam', 'adam',1);
insert into authenticate (user_id,login, password,Profile_status) values (4,'dan', 'dan',0);
insert into authenticate (user_id,login, password,Profile_status) values (5,'block', 'block',1);

/******************** Add Table: Books_genre ************************/
INSERT INTO Books_genre (Genre) VALUES ('Programming Java');
INSERT INTO Books_genre (Genre) VALUES ('Programming C++');

INSERT INTO Books (Tittle, Year, Genre_id, Reserved, Count) VALUES ('Clean Code: A Handbook of Agile Software Craftsmanship 1st Edition', 2008, 1,false, 5);
INSERT INTO Books (Tittle, Year, Genre_id, Reserved, Count) VALUES ('Effective Java', 2018, 1,false, 5);
INSERT INTO Books (Tittle, Year, Genre_id, Reserved, Count) VALUES ('Microservices Patterns: With examples in Java', 2018, 1,false, 5);
INSERT INTO Books (Tittle, Year, Genre_id, Reserved, Count) VALUES ('Java: The Complete Reference, Eleventh Edition', 2018, 2,false, 5);

/******************** Add Table: Authors ************************/
INSERT INTO Authors (name, surname) VALUES ('Robert', 'C. Martin');
INSERT INTO Authors (name, surname) VALUES ('Joshua', 'Bloch');
INSERT INTO Authors (name, surname) VALUES ('Chris', 'Richardson');
INSERT INTO Authors (name, surname) VALUES ('Herbert', 'Schildt');

/******************** Add Table: Books_authors ************************/
INSERT INTO Books_authors (Author_id, Book_id) VALUES (1, 1);
INSERT INTO Books_authors (Author_id, Book_id) VALUES (2, 2);
INSERT INTO Books_authors (Author_id, Book_id) VALUES (3, 3);
INSERT INTO Books_authors (Author_id, Book_id) VALUES (4, 4);

/******************** Add Table: Buckets ************************/
INSERT INTO Buckets (id, user_id, book_id, reservedDays) VALUES (1,5,1,'2019-12-31');

/******************** Add Table: Buckets ************************/
INSERT INTO BOOKS_IMAGES (bookId, bookImage) VALUES (0,FILE_READ('/no-image.jpg'));

