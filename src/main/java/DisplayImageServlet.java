import repository.BookImageDaoImpl;
import entity.BookImage;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DisplayImageServlet", urlPatterns = "/DisplayImageServlet")
public class DisplayImageServlet extends HttpServlet{

    public void doGet(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType("image/jpeg");
        int bookId = Integer.parseInt(req.getParameter("bookId"));
        try {
            BookImageDaoImpl bookImageDao = new BookImageDaoImpl();
            BookImage bookImage;
            try {
                bookImage = bookImageDao.getImage(bookId);
            } catch (Exception e) {
                bookImage = null;
            }
            byte[] buffer;
            if (bookImage == null) {
                buffer = bookImageDao.getById(1).getBookImage();
            } else {
                buffer = bookImage.getBookImage();
            }
            resp.setContentLength(buffer.length);
            resp.getOutputStream().write(buffer);

        } catch (Exception e) {
            e.printStackTrace();
        }
        }
}
