package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "BUCKETS")
public class Bucket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;
    public Book getBook() {
        return book;
    }
    public void setBook(Book book) {
        this.book = book;
    }

    private Date reservedDays;

    public Bucket() {
    }

    public Bucket(User user, Book book, Date reservedDays) {
        this.user = user;
        this.book = book;
        this.reservedDays = reservedDays;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getReservedDays() {
        return reservedDays;
    }

    public void setReservedDays(Date reservedDays) {
        this.reservedDays = reservedDays;
    }
}
