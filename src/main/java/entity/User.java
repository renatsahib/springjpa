package entity;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "surname", length = 50)
    private String surname;

    @Column(name = "age")
    private int age;

    @Column(name = "email", nullable = false, unique = true, length = 50)
    private String email;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "role")
    private UserRole userRole;
    public UserRole getUserRole() {
        return userRole;
    }
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

//    @Enumerated(EnumType.STRING)
//    @Column(columnDefinition="enum('ACTIV','TEMP_BlOCK','BLOCK')")
//    private ProfileStatus profileStatus;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
    private Authenticate authenticate;
    public Authenticate getAuthenticate() {
        return authenticate;
    }
    public void setAuthenticate(Authenticate authenticate) {
        this.authenticate = authenticate;
    }

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
    private BlockedTime blockedTime;
    public BlockedTime getBlockedTime() {
        return blockedTime;
    }
    public void setBlockedTime(BlockedTime blockedTime) {
        this.blockedTime = blockedTime;
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    private Set<Bucket> bucketSet;
    public Set<Bucket> getBucketSet() {
        return bucketSet;
    }
    public void setBucketSet(Set<Bucket> bucketSet) {
        this.bucketSet = bucketSet;
    }

    public User(){}

    public User(String name, String surname, int age, String email) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.email = email;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//
//    public ProfileStatus getProfileStatus() {
//        return profileStatus;
//    }
//
//    public void setProfileStatus(ProfileStatus profileStatus) {
//        this.profileStatus = profileStatus;
//    }

}
