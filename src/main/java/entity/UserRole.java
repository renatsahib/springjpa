package entity;

public enum UserRole {
    GUEST, USER, ADMIN;

    UserRole() {
    }
}
