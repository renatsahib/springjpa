package entity;

import javax.persistence.*;

@Entity
@Table(name = "BOOKS_IMAGES")
public class BookImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long bookId;
    @Lob
    private byte[] bookImage;

//    @OneToOne
//    @MapsId
//    private Book book;
//    public Book getBook() {
//        return book;
//    }
//    public void setBook(Book book) {
//        this.book = book;
//    }

    public BookImage() {
    }

//    public BookImage(long bookId, byte[] bookImage, Book book) {
//        this.bookId = bookId;
//        this.bookImage = bookImage;
//        this.book = book;
//    }

    public BookImage(long bookId, byte[] bookImage) {
        this.bookId = bookId;
        this.bookImage = bookImage;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getBookImage() {
        return bookImage;
    }

    public void setBookImage(byte[] bookImage) {
        this.bookImage = bookImage;
    }
}

