package entity;

import javax.persistence.*;

@Entity
@Table(name = "AUTHENTICATE")
public class Authenticate {

    @Id
    @Column(name = "id")
    private long id;

    private String login;
    private String password;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "Profile_status")
    private ProfileStatus profileStatus;
    public void setProfileStatus(ProfileStatus profileStatus) {
        this.profileStatus = profileStatus;
    }
    public ProfileStatus getProfileStatus() {
        return profileStatus;
    }


    @OneToOne
    @MapsId
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Authenticate() {
    }

    public Authenticate(String login, String password, ProfileStatus profileStatus) {
        this.login = login;
        this.password = password;
        this.profileStatus = profileStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }


    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
