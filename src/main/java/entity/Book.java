package entity;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "BOOKS")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String tittle;
    private int year;
    private boolean reserved = false;
    private int count = 0;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "genre_id", referencedColumnName = "id")
    private BookGenre bookGenre;
    public BookGenre getBookGenre() {
        return bookGenre;
    }
    public void setBookGenre(BookGenre bookGenre) {
        this.bookGenre = bookGenre;
    }

    @OneToMany(mappedBy = "book", fetch = FetchType.EAGER)
    private Set<BookAuthor> bookAuthorSet;
    public Set<BookAuthor> getBookAuthorSet() {
        return this.bookAuthorSet;
    }
    public void setBookAuthorSet(Set<BookAuthor> bookAuthorSet) {
        this.bookAuthorSet = bookAuthorSet;
    }

    @OneToMany(mappedBy = "book", cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Bucket> bucketSet;
    public Set<Bucket> getBucketSet() {
        return bucketSet;
    }
    public void setBucketSet(Set<Bucket> bucketSet) {
        this.bucketSet = bucketSet;
    }

    public Book() {
    }

    public Book(long id, String tittle, int year, boolean reserved, int count) {
        this.id = id;
        this.tittle = tittle;
        this.year = year;
        this.reserved = reserved;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
}