package entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "MESSAGES")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long fromUserId;
    private long toUserId;
    private String userMessage;
    private Timestamp time;

    public Message() {
    }

    public Message(long fromUserId, long toUserId, String userMessage, Timestamp time) {

        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.userMessage = userMessage;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return userMessage;
    }

    public void setMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
