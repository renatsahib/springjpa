package entity;

import javax.persistence.*;

@Entity
@Table(name = "BOOKS_AUTHORS")
public class BookAuthor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne()
    @JoinColumn(name = "author_id")
    private Author author;
    public Author getAuthor() {
        return author;
    }
    public void setAuthor(Author author) {
        this.author = author;
    }

    @ManyToOne()
    @JoinColumn(name = "book_id")
    private Book book;
    public Book getBook() {
        return book;
    }
    public void setBook(Book book) {
        this.book = book;
    }

    public BookAuthor() {
    }

    public BookAuthor(Author author, Book book) {
        this.author = author;
        this.book = book;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
}