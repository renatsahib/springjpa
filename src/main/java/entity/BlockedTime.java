package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "BLOCKED_TIME")
public class BlockedTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long userId;
    private Date blockedTime;

    @OneToOne
    @MapsId
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public BlockedTime() {
    }

    public BlockedTime(long userId, Date blockedTime, User user) {
        this.userId = userId;
        this.blockedTime = blockedTime;
        this.user = user;
    }
    //    public BlockedTime(long userId, Date blockedTime) {
//        this.userId = userId;
//        this.blockedTime = blockedTime;
//    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getBlockedTime() {
        return blockedTime;
    }

    public void setBlockedTime(Date blockedTime) {
        this.blockedTime = blockedTime;
    }
}
