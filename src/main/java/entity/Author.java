package entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "AUTHORS")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String surname;

    @OneToOne(mappedBy = "author")
    BookAuthor bookAuthorSet;
    public BookAuthor getBookAuthorSet() {
        return bookAuthorSet;
    }
    public void setBookAuthorSet(BookAuthor bookAuthorSet) {
        this.bookAuthorSet = bookAuthorSet;
    }
//    Set<BookAuthor> bookAuthorSet;
//    public Set<BookAuthor> getBookAuthorSet() {
//        return bookAuthorSet;
//    }
//    public void setBookAuthorSet(Set<BookAuthor> bookAuthorSet) {
//        this.bookAuthorSet = bookAuthorSet;
//    }

    public Author() {
    }

//    public Author(String name, String surname) {
//        this.name = name;
//        this.surname = surname;
//    }

     public Author(String name, String surname, BookAuthor bookAuthorSet) {
        this.name = name;
        this.surname = surname;
        this.bookAuthorSet = bookAuthorSet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


}

