import repository.BlockedTimeDaoImpl;
import entity.Authenticate;
import entity.BlockedTime;
import entity.Bucket;
import entity.User;
import repository.BucketDaoImpl;
import repository.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static entity.ProfileStatus.ACTIV;
import static entity.ProfileStatus.BLOCK;
import static entity.ProfileStatus.TEMP_BlOCK;

@WebServlet (name = "ChangeProfileStatusServlet", urlPatterns = "/ChangeProfileStatusServlet", loadOnStartup = 0)
public class ChangeProfileStatusServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        UserDaoImpl userDao = new UserDaoImpl();
        BucketDaoImpl bucketDao = new BucketDaoImpl();
        BlockedTimeDaoImpl blockedTimeDao = new BlockedTimeDaoImpl();
        Authenticate authenticate;
        User lockUser;
        long userId;
        BlockedTime blockedTime;

        //Unlock User
        if(req.getParameter("action").equals("unlockUser")) {
            userId = Long.parseLong(req.getParameter("unlockUser"));
            try {
                lockUser = userDao.getById(userId);
                authenticate = lockUser.getAuthenticate();
                authenticate.setProfileStatus(ACTIV);
                userDao.update(lockUser);
                blockedTimeDao.removeBlockedTime(lockUser.getId());
                req.setAttribute("userList", userDao.getAll()); //set to request new user ArrayList

                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);

                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Lock User
        if(req.getParameter("action").equals("lockUser")) {
            userId = Long.parseLong(req.getParameter("lockUser"));
            List<Bucket> listBucket;
            try {
                lockUser = userDao.getById(userId);
                authenticate = lockUser.getAuthenticate();
                authenticate.setProfileStatus(BLOCK);
                listBucket = bucketDao.getBucket(userId);
                for(Bucket bucket1:listBucket) {
                    bucketDao.returnBook(bucket1.getBook().getId(), userId);
                }
                userDao.update(lockUser);
                req.setAttribute("userListBucket", bucketDao.getAllBucket());
                req.setAttribute("userList", userDao.getAll()); //set to request new user ArrayList
                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Temp Lock User
        if(req.getParameter("action").equals("tempLockUser")) {
            userId = Long.parseLong(req.getParameter("tempLockUser"));
            lockUser =  userDao.getById(userId);
            int blockedTimeInt = Integer.parseInt(req.getParameter("blockedTimeUser"));
            java.sql.Date now = new java.sql.Date( new java.util.Date().getTime() );
            java.sql.Date blockedTimeUser= new java.sql.Date( now.getTime() + blockedTimeInt*(24*60*60*1000));

            try {
                authenticate = lockUser.getAuthenticate();
                authenticate.setProfileStatus(TEMP_BlOCK);
                blockedTime = new BlockedTime(userId,blockedTimeUser,lockUser);
                lockUser.setBlockedTime(blockedTime);
                userDao.update(lockUser);
                req.setAttribute("userList", userDao.getAll()); //set to request new user ArrayList

                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);

                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
