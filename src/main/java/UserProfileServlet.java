import dao.*;
import entity.Author;
import entity.Book;
import entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "UserProfileServlet", urlPatterns = "/UserProfileServlet", loadOnStartup = 0)
public class UserProfileServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        long userId = Long.parseLong(req.getParameter("userId"));
        req.setAttribute("userId", userId);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_profile.jsp");
        requestDispatcher.forward(req, resp);
    }

}
