/******************** Add Table: Users ************************/
CREATE TABLE Users
(
  Id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
  Name VARCHAR(50) NOT NULL,
  Surname VARCHAR(50),
  Email VARCHAR(50),
  Age INTEGER
);

/******************** Add Table: Users_roles ************************/
CREATE TABLE Users_roles
(
  Id INTEGER AUTO_INCREMENT NOT NULL,
  User_id INTEGER NOT NULL,
  Role_id INTEGER NOT NULL
);

/******************** Add Table: Roles ************************/
CREATE TABLE Roles
(
  id INTEGER NOT NULL,
  Role VARCHAR(50) NOT NULL
);

/******************** Add Table: Profile_status ************************/
CREATE TABLE Profile_status
(
  Id INTEGER NOT NULL,
  Profile_status VARCHAR(50) NOT NULL
);

/******************** Add Table: Authenticate ************************/

CREATE TABLE Authenticate
(
  Id INTEGER AUTO_INCREMENT NOT NULL,
  Login VARCHAR(50) NOT NULL,
  Password VARCHAR(50) NOT NULL,
  Profile_status INTEGER NOT NULL
);

/************ Add Foreign Keys: Authenticate ***************/
/* Add Foreign Key: id */
ALTER TABLE Authenticate ADD CONSTRAINT Authenticate_id
	FOREIGN KEY (id) REFERENCES Users (Id)
	ON UPDATE NO ACTION ON DELETE CASCADE;
	
/************ Add Foreign Keys: Authenticate ***************/
/* Add Foreign Key: Profile_status */
ALTER TABLE Authenticate ADD CONSTRAINT Profile_status
	FOREIGN KEY (Profile_status) REFERENCES Profile_status (id)
	ON UPDATE NO ACTION ON DELETE NO ACTION;
	

/************ Add Foreign Keys: Users_roles ***************/
/* Add Foreign Key: id */
ALTER TABLE Users_roles ADD CONSTRAINT id
	FOREIGN KEY (User_id) REFERENCES Users (Id)
	ON UPDATE NO ACTION ON DELETE CASCADE;

/* Add Foreign Key: role_id */
ALTER TABLE Users_roles ADD CONSTRAINT role_id
	FOREIGN KEY (Role_id) REFERENCES Roles (id)
	ON UPDATE NO ACTION ON DELETE NO ACTION;
	
/******************** Add Data: User ************************/
insert into users (name , surname , age , email ) values ('alan','smith',18,'a@ya.ru');
insert into users (name , surname , age , email ) values ('marcus','rashford',23,'b@ya.ru');
insert into users (name , surname , age , email ) values ('daniel','james',21,'c@ya.ru');
insert into users (name , surname , age , email ) values ('dan','greenwood',19,'d@ya.ru');


/******************** Add Data: Roles ************************/
insert into ROLES (id, role) values (1, 'guest');
insert into ROLES (id, role) values (2, 'user');
insert into ROLES (id, role) values (3, 'admin');

/******************** Add Data: Profile_status ************************/
insert into Profile_status (id, Profile_status ) values (1,'activ');
insert into Profile_status (id, Profile_status ) values (2,'tempBlock');
insert into Profile_status (id, Profile_status ) values (3,'block');

/******************** Add Data: Users_roles ************************/
insert into users_roles (user_id , role_id ) values (1,2);
insert into users_roles (user_id , role_id ) values (2,3);
insert into users_roles (user_id , role_id ) values (3,2);
insert into users_roles (user_id , role_id ) values (4,1);

/******************** Add Data: Authenticate ************************/
insert into authenticate (login, password,Profile_status) values ('user', 'user',1);
insert into authenticate (login, password,Profile_status) values ('admin', 'admin',1);
insert into authenticate (login, password,Profile_status) values ('adam', 'adam',2);
insert into authenticate (login, password,Profile_status) values ('dan', 'dan',1);


CREATE TABLE Books
(
  Id BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  Tittle VARCHAR(100) NOT NULL,
  Year INTEGER NOT NULL,
  Genre_id BIGINT NOT NULL,
  Reserved BOOLEAN NOT NULL,
  Book_time_reserved DATETIME NULL,
  Count INTEGER NOT NULL
);

INSERT INTO Books
(Tittle, Year, Genre_id, Reserved, Book_time_reserved, Count)
VALUES
('Clean Code: A Handbook of Agile Software Craftsmanship 1st Edition',2019,1,false, null, 5);
INSERT INTO Books
(Tittle, Year, Genre_id, Reserved, Book_time_reserved, Count)
VALUES
  ('Effective Java',2018,1,false, null, 5);
INSERT INTO Books
(Tittle, Year, Genre_id, Reserved, Book_time_reserved, Count)
VALUES
  ('Microservices Patterns: With examples in Java',2018,1,false, null, 5);
INSERT INTO Books
(Tittle, Year, Genre_id, Reserved, Book_time_reserved, Count)
VALUES
  ('Java: The Complete Reference, Eleventh Edition',2018,1,false, null, 5);

