import init.InitBookForServlet;
import repository.BookDaoImpl;
import repository.BucketDaoImpl;
import entity.Book;
import entity.Bucket;
import entity.User;
import repository.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "BucketServlet", urlPatterns = "/BucketServlet", loadOnStartup = 0)
public class BucketServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        //Add book to bucket
        if (req.getParameter("action").equals("AddToBucket")) {
            BucketDaoImpl bucketDao = new BucketDaoImpl();
            User user = (User) req.getSession().getAttribute("userSession");
            Book book = (Book) req.getSession().getAttribute("bookSession");
            int reservedDays = Integer.parseInt(req.getParameter("reserved_days"));
            java.sql.Date now = new java.sql.Date( new java.util.Date().getTime() );
            java.sql.Date sqlDate= new java.sql.Date( now.getTime() + reservedDays*(24*60*60*1000));
//            BookDaoImpl bookDao = new BookDaoImpl();
//            List<Bucket> listBucket;
//            List<Book> listBook = new ArrayList<>();
            try {
                bucketDao.addBookToBucket(user, book, sqlDate);
                UserDaoImpl userDao = new UserDaoImpl();
                user = userDao.getById(user.getId());
                req.setAttribute("user", user);
                new InitBookForServlet(book.getId(),req, resp);
//                listBucket = bucketDao.getBucket(user.getId());
//                for(Bucket bucket: listBucket){
//                    Book books;
//                    books = bookDao.getById(bucket.getBookID());
//                    books = bookDao.getById(bucket.getBook().getId());
//                    listBook.add(books);
//                }
//                req.setAttribute("bucket", listBucket);
//                req.setAttribute("book", listBook);
                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //Return Book
        if(req.getParameter("action").equals("returnBook")){
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("userSession");
            long userId = user.getId();
            BucketDaoImpl bucketDao = new BucketDaoImpl();
            String id = req.getParameter("bookId");
            try {
                bucketDao.returnBook(Long.parseLong(id), userId);
//                BookDaoImpl bookDao = new BookDaoImpl();
//                List<Bucket> listBucket;
//                List<Book> listBook = new ArrayList<>();
//                listBucket = bucketDao.getBucket(user.getId());
//                for (Bucket bucket : listBucket) {
//                    Book books;
////                    books = bookDao.getById(bucket.getBookID());
//                    books = bookDao.getById(bucket.getBook().getId());
//                    listBook.add(books);
//                }
                UserDaoImpl userDao = new UserDaoImpl();
                user = userDao.getById(userId);
                req.setAttribute("user", user);

//                req.setAttribute("bucket", listBucket);
//                req.setAttribute("book", listBook);
                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);
//            bucketDao.updateBucket(user,req,resp);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


            //Get book from bucket
        if (req.getParameter("action").equals("GetBucket")) {



        }

    }


}
