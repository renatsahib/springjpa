import init.InitListBookForServlet;
import repository.BookDaoImpl;
import entity.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "IndexServlet", urlPatterns = "/IndexServlet", loadOnStartup = 0)
public class IndexServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("UTF-8");

        BookDaoImpl bookDao = new BookDaoImpl();
        List<Book> listBookDao;
        HttpSession session = req.getSession();

        try {
            new InitListBookForServlet(req,resp);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/index.jsp");
            requestDispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("UTF-8");

        BookDaoImpl bookDao = new BookDaoImpl();
        List<Book> listBookDao;
        HttpSession session = req.getSession();
        try {
            listBookDao = bookDao.getAll();
            session.setAttribute("bookListSession", listBookDao); //Set bookList to Session
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/index.jsp");
            requestDispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
