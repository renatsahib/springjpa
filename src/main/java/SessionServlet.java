import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Book;
import entity.Bucket;
import entity.User;

import java.io.IOException;


@WebServlet(name = "SessionServlet", urlPatterns = "/SessionServlet")
public class SessionServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
//        session.setMaxInactiveInterval(600);
        User userSession = (User) req.getAttribute("user");

        if (session.getAttribute("userSession") == null) {
            session.setAttribute("userSession", userSession);
        }

        if (req.getAttribute("user") != null){
            session.setAttribute("userSession", req.getAttribute("user"));
        }
        //Set user to Session
        if (req.getAttribute("bucket") != null) {
                session.setAttribute("userBookSession", req.getAttribute("book")); //Set user book to Session
                session.setAttribute("userBucketSession", req.getAttribute("bucket")); //Set user bucket to Session
        }

        if (req.getAttribute("listBooks") != null) {
            session.setAttribute("bookListSession", req.getAttribute("listBooks")); //Set bookList to Session
        }

        if (req.getAttribute("userList") != null) {
            session.setAttribute("userListSession", req.getAttribute("userList")); //Set userList to Session
//            session.setAttribute("userListAuthenticateSession", req.getAttribute("userListAuthenticate")); //Set userListAuthenticate to Session
        }

        if (req.getAttribute("userListBucket") != null) {
            session.setAttribute("userListBucketSession", req.getAttribute("userListBucket")); //Set AllBucket to Session
        }

        req.getSession();
    }
}
