package repository;

import entity.User;
import entity.UserRole;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class UsersRolesDaoImpl implements UsersRolesDao {

    @Override
    public String getUserRole (long userID) {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            User user = em.find(User.class, userID);
            em.getTransaction().commit();
            em.close();
            return user.getUserRole().name();
    }

    @Override
    public void setUserRole (long userId, Enum<UserRole> roleEnum) {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            User user = em.find(User.class, userId);
            user.setUserRole((UserRole) roleEnum);
            em.getTransaction().commit();
            em.close();
    }

    @Override
    public List<Long> getAdminId() {
            EntityManager em = getEntityManager();
            List<Long> userAdmin = em.createQuery("select u.id from User u where u.userRole = 2").getResultList();
            em.close();
            return userAdmin;
    }
}
