package repository;

import entity.BookImage;
import entity.Message;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDaoImpl extends AbstractGenericRepository<Message> implements MessageDao,  GenericRepository<Message> {

    @Override
    public List<Message> getInputMessage(long userId) {
        EntityManager em = getEntityManager();
        List<Message> messageList = em.createQuery("SELECT m FROM Message m where m.toUserId = ?1", Message.class)
                .setParameter(1,userId)
                .getResultList();
        em.close();
        return messageList;
    }

    @Override
    public List<Message> getOutputMessage(long userId) {
        EntityManager em = getEntityManager();
        List<Message> messageList = em.createQuery("SELECT m FROM Message m where m.fromUserId = ?1", Message.class)
                .setParameter(1,userId)
                .getResultList();
        em.close();
        return messageList;
    }

}
