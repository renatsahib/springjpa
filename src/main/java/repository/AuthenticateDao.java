package repository;

import java.sql.SQLException;

public interface AuthenticateDao {
    
    long checkLogin(String login) throws SQLException;
}
