package repository;

public interface BookDao {

    long getMaxBookId();
    void setBookReserved(long bookId);
    void unsetBookReserved(long bookId);
    void setBookCount(long booId, int count);
    int getBookCount(long bookId);
}
