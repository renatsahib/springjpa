package repository;

import org.h2.tools.Server;
import util.JpaEntityManagerFactoryUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractGenericRepository<T> implements GenericRepository<T> {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = JpaEntityManagerFactoryUtil.getEntityManagerFactory();

    private final Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    protected EntityManager getEntityManager() {
        return ENTITY_MANAGER_FACTORY.createEntityManager();
    }

    private static final Server SERVER;

    static {
        try {
            SERVER = Server.createTcpServer().start();
        } catch (SQLException e) {
            throw new RuntimeException("Failed start tcp H2 server");
        }
    }

    @Override
    public T save(T t) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(t);
        em.getTransaction().commit();
        em.close();
        return t;
    }

    @Override
    public T getById(long id) {
        EntityManager em = getEntityManager();
        T t = em.find(clazz, id);
        em.close();
        return t;
    }

    @Override
    public void delete(long id) {
        EntityManager em = getEntityManager();
        T t = em.find(clazz, id);
        em.getTransaction().begin();
        em.remove(t);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public T update(T t) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        t = em.merge(t);
        em.getTransaction().commit();
        em.close();
        return t;
    }

    @Override
    public List<T> getAll() {
        EntityManager em = getEntityManager();
        List<T> list = em.createQuery(String.format("select t from %s t", clazz.getName()), clazz)
                .getResultList();
        em.close();
        return list;
    }

    protected T getSingleResultByQuery(String query) {
        EntityManager em = getEntityManager();
        T t = em.createQuery(query, clazz).getSingleResult();
        em.close();
        return t;
    }

    protected List<T> getResultListByQuery(String query) {
        EntityManager em = getEntityManager();
        List<T> t = em.createQuery(query, clazz).getResultList();
        em.close();
        return t;
    }
}
