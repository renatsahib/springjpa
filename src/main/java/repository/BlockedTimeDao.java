package repository;

import entity.BlockedTime;
import entity.User;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface BlockedTimeDao {

    void addBlockedTime(long userId, Date blockedTime, User user) throws SQLException;
    void removeBlockedTime(long userId) throws SQLException;
//    BlockedTime getBlockedTime(long userId) throws SQLException;
//    List<BlockedTime> getAllBlockedTime() throws SQLException;

}
