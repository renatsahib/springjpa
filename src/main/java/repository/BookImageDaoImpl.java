package repository;

import entity.BookImage;
import javax.persistence.EntityManager;

public class BookImageDaoImpl extends AbstractGenericRepository<BookImage> implements GenericRepository<BookImage>, BookImageDao{

    public void addImage(long bookId, byte[] image) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("insert into BookImage (bookId, bookImage) select ?1,?2 from BookImage ")
                .setParameter(1,bookId)
                .setParameter(2,image)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    public BookImage getImage(long bookId) {
        EntityManager em = getEntityManager();
        BookImage bookImage = em.createQuery("SELECT b FROM BookImage b where b.bookId = ?1", BookImage.class)
                .setParameter(1,bookId)
                .getSingleResult();
        em.close();
        return bookImage;
    }

    public void deleteImage(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE BookImage b WHERE b.bookId = ?1")
                .setParameter(1, bookId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
