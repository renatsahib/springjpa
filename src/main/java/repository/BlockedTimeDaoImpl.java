package repository;

import dao.AbstractCrudDao;
import entity.*;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class BlockedTimeDaoImpl extends AbstractGenericRepository<BlockedTime> implements GenericRepository<BlockedTime>, BlockedTimeDao{

    @Override
    public void addBlockedTime(long userId, Date blockedTime, User user) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("insert into BlockedTime (userId, blockedTime, User ) select ?1,?2,?3 from BlockedTime")
                .setParameter(1,userId)
                .setParameter(2,blockedTime)
                .setParameter(3,user)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeBlockedTime(long userId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE BlockedTime b WHERE b.userId = ?1")
                .setParameter(1, userId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }


}
