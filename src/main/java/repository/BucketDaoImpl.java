package repository;

import entity.Book;
import entity.Bucket;
import entity.User;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.List;
import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class BucketDaoImpl implements BucketDao{

    public Bucket getBucketById(long bucketId) {
        EntityManager em = getEntityManager();
        Bucket t = em.find(Bucket.class, bucketId);
        em.close();
        return t;
    }

    @Override
    public List<Bucket> getBucket(long userId) {
        EntityManager em = getEntityManager();
        List<Bucket> list = em.createQuery("select b from Bucket b where b.user.id = ?1", Bucket.class)
                .setParameter(1,userId)
                .getResultList();
        em.close();
        return list;
    }


    public List<Bucket> getBucketByBookId(long bookId) {
        EntityManager em = getEntityManager();
        List<Bucket> list = em.createQuery("SELECT b FROM Bucket b where b.book.id = ?1", Bucket.class)
                .setParameter(1,bookId)
                .getResultList();
        em.close();
        return list;
    }

    public void deleteBucketByBookId(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE Bucket b WHERE b.book.id = ?1")
                .setParameter(1,bookId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }


    public List<Bucket> getAllBucket() {
        EntityManager em = getEntityManager();
        List<Bucket> list = em.createQuery("select b from Bucket b", Bucket.class)
                .getResultList();
        em.close();
        return list;
    }

    @Override
    public void addBookToBucket(User user, Book book, Date reservedDays) {
        Bucket bucket = new Bucket(user, book, reservedDays);
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(bucket);
        em.merge(bucket);
        BookDaoImpl reserved = new BookDaoImpl();
        int count = book.getCount() - 1;
        book.setCount(count);
        if (count == 0) {
            reserved.setBookReserved(book.getId());
        }
        em.merge(book);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void returnBook(long bookId, long userId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("delete from Bucket b WHERE b.book.id = ?2 AND b.user.id = ?1")
                .setParameter(1, userId)
                .setParameter(2, bookId)
                .executeUpdate();
        BookDaoImpl reserved = new BookDaoImpl();
        int count;
        if (reserved.getBookCount(bookId) == 0) {
            reserved.unsetBookReserved(bookId);
        }
        count = reserved.getBookCount(bookId) + 1;
        reserved.setBookCount(bookId, count);
        em.getTransaction().commit();
        em.close();
    }


//    public void updateBucket(User user, HttpServletRequest req, HttpServletResponse resp) throws SQLException, IOException {
//        try (Connection connection = DataBaseConnector.getInstance().getConnection();
////             PreparedStatement stmt = connection.prepareStatement(returnBookSql())
//        ) {
//            BucketDaoImpl bucketDao = new BucketDaoImpl();
////            User user = (User) req.getSession().getAttribute("userSession");
//            BookDaoImpl bookDao = new BookDaoImpl();
//            List<Bucket> listBucket;
//            List<Book> listBook = new ArrayList<>();
//            listBucket = bucketDao.getBucket(user.getId());
//
//            for (Bucket bucket : listBucket) {
//                Book books;
////                books = bookDao.getById(bucket.getBookID());
//                books = bookDao.getById(bucket.getBook().getId());
//                listBook.add(books);
//            }
//            req.setAttribute("bucket", listBucket);
//            req.setAttribute("book", listBook);
//            RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
//            requestDispatcherSession.include(req, resp);
//            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
//            requestDispatcher.forward(req, resp);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        }
//    }
    @Override
    public void setReservedTime(Date day) {

    }


}
