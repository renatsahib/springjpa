package repository;

import entity.ProfileStatus;

import java.sql.SQLException;

public interface ProfileStatusDao {
    ProfileStatus getProfileStatus(long userId);
    void updateProfileStatus(long userId, Enum<ProfileStatus> profileStatusEnum);
}
