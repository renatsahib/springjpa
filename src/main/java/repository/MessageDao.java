package repository;

import entity.Message;

import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

public interface MessageDao {
    List<Message> getInputMessage(long userId);
    List<Message> getOutputMessage(long userId);
}
