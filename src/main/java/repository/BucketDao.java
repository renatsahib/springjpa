package repository;

import entity.Book;
import entity.Bucket;
import entity.User;

import java.sql.SQLException;
import java.sql.Date;
import java.util.List;

public interface BucketDao {

    List<Bucket> getBucket(long userId);
    List<Bucket> getAllBucket();
    void addBookToBucket(User user, Book book, Date ReservedDays);
    void returnBook(long bookId, long userId);
    void setReservedTime(Date day);

}
