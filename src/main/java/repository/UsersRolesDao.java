package repository;

import entity.User;
import entity.UserRole;

import java.sql.SQLException;
import java.util.List;

public interface UsersRolesDao {
    String getUserRole(long userId) throws SQLException;
    void setUserRole(long userId, Enum<UserRole> roleEnum) throws SQLException;
    List getAdminId() throws SQLException;
}
