package repository;

import entity.Author;

import java.sql.SQLException;
import java.util.List;

public interface BookAuthorDao {
    List<Author> getBookAuthor(long bookId);
    void setBookAuthor(long bookId, List<Long> authorsId);
    void updateBookAuthor (long bookId, List<Long> authorsId);
    long getMaxBookId();
    public void removeBookAuthor(long bookId);
}
