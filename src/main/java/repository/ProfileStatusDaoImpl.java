package repository;

import entity.*;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class ProfileStatusDaoImpl implements ProfileStatusDao{

    @Override
    public ProfileStatus getProfileStatus (long userID) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        User user = em.find(User.class, userID);
        em.getTransaction().commit();
        em.close();
        return user.getAuthenticate().getProfileStatus();
    }

    @Override
    public void updateProfileStatus (long userId, Enum<ProfileStatus> profileStatusEnum) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("update Authenticate a set a.profileStatus = ?1 where a.user.id= ?2")
                .setParameter(1,profileStatusEnum)
                .setParameter(2,userId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
