package repository;

import entity.User;

import javax.persistence.EntityManager;
import java.sql.SQLException;

public class UserDaoImpl extends AbstractGenericRepository<User> implements GenericRepository<User>, UserDao{

//    private String getUserIdSql() {return "SELECT MAX(`Id`) FROM `USERS`";}

    @Override
    public long getUserId() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        User user = em.createQuery("select User FROM User where User.id = max", User.class).getSingleResult();
        long userId = user.getId();
        em.getTransaction().commit();
        em.close();
        return userId;
    }
}

