package repository;

public interface AuthorDao {

    long getAuthorId();
}
