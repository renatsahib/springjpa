package repository;

import entity.Book;
import javax.persistence.EntityManager;


public class BookDaoImpl extends AbstractGenericRepository<Book> implements GenericRepository<Book>, BookDao{

    @Override
    public long getMaxBookId() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.createQuery("select max(Book.id) FROM Book ", Book.class).getSingleResult();
        long bookId = book.getId();
        em.getTransaction().commit();
        em.close();
        return bookId;
    }

    @Override
    public void setBookReserved(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        book.setReserved(true);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void unsetBookReserved(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        book.setReserved(false);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void setBookCount(long bookId, int count) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        book.setCount(count);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public int getBookCount(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        int count = book.getCount();
        em.getTransaction().commit();
        em.close();
        return count;
    }
}
