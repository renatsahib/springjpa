package repository;
import entity.Authenticate;
import entity.User;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.math.BigInteger;

public class AuthenticateDaoImpl extends AbstractGenericRepository<Authenticate> implements GenericRepository<Authenticate>, AuthenticateDao {

//    @Override
//    public long checkLogin(String login) {
//        EntityManager em = getEntityManager();
////        User user = em.createNativeQuery("select id from USERS  where User.authenticate.login = ?1 ", User.class)
//        BigInteger userId = (BigInteger) em.createNativeQuery("SELECT USERS.ID as User_Id from USERS JOIN AUTHENTICATE ON (users.id = AUTHENTICATE.user_id) where AUTHENTICATE.LOGIN = ?1")
//                    .setParameter(1,login)
//                    .getSingleResult();
//        long id = userId.longValue();
//
////        long id;
////        if (user != null) {
////            id = user.getId();
////        } else {
////            id = -1;
////        }
//        em.close();
//        return id;
//    }

    @Override
    public long checkLogin(String login) {
        EntityManager em = getEntityManager();
        long id;
        try {
            User user = em.createQuery("select u from User u where u.authenticate.login = ?1 ", User.class)
                    .setParameter(1,login)
                    .getSingleResult();
            id = user.getId();

        } catch (NoResultException nre) {
//            if (nre.getMessage().equals("No entity found for query")) {
//                id = -1;
//            }
            id = -1;
        }
        System.out.println(id);
        em.close();
        return id;
    }

}


