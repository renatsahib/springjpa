package repository;

import dao.AbstractCrudDao;
import dao.CrudDao;
import entity.Author;
import entity.User;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDaoImpl extends AbstractGenericRepository<Author> implements GenericRepository<Author>, AuthorDao{

//    private String getBookIdSql() {return "SELECT MAX(`Id`) FROM `AUTHORS`";}

    @Override
    public long getAuthorId() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Author author = em.createQuery("select max(Id) FROM Author", Author.class).getSingleResult();
        long authorId = author.getId();
        em.getTransaction().commit();
        em.close();
        return authorId;
    }

}
