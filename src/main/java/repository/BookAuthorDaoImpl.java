package repository;

import entity.Author;
import entity.Book;

import javax.persistence.EntityManager;
import java.util.List;

import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class BookAuthorDaoImpl implements BookAuthorDao{

    @Override
    public List<Author> getBookAuthor (long bookId) {
        EntityManager em = getEntityManager();
        List<Author> authorList = em.createQuery("SELECT BookAuthor.author.id FROM BookAuthor where BookAuthor.book.id = ?1", Author.class)
                .setParameter(1,bookId)
                .getResultList();
        em.close();
        return authorList;
    }

    @Override
    public void setBookAuthor (long bookId, List<Long> authorsId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        for (Long authorId : authorsId) {
                 Author author = em.find(Author.class, authorId);
                 em.createQuery("insert into BookAuthor (book, author) select ?1,?2 from BookAuthor")
                .setParameter(1,book)
                .setParameter(2,author)
                .executeUpdate();
        }
        em.getTransaction().commit();
        em.close();
    }

    public void setBookAuthors (long bookId, long authorsId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Book book = em.find(Book.class, bookId);
        Author author = em.find(Author.class, authorsId);
        em.createQuery("insert into BookAuthor (book, author) select ?1,?2 from BookAuthor")
                    .setParameter(1,book)
                    .setParameter(2,author)
                    .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void updateBookAuthor (long bookId, List<Long> authorsId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        removeBookAuthor(bookId);
        setBookAuthor(bookId, authorsId);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public long getMaxBookId() {
        EntityManager em = getEntityManager();
        Book book = em.createQuery("select max(Book.id) FROM Book", Book.class).getSingleResult();
        long bookId = book.getId();
        em.close();
        return bookId;
    }

    @Override
    public void removeBookAuthor(long bookId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
//        Book book = em.find(Book.class, bookId);
        em.createQuery("DELETE BookAuthor b WHERE b.book.id = ?1")
                .setParameter(1, bookId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
