import entity.Message;
import entity.User;
import repository.MessageDaoImpl;
import repository.UsersRolesDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet(name = "MessageServlet", urlPatterns = "/MessageServlet")
public class MessageServlet extends HttpServlet{
    private MessageDaoImpl messageDao = new MessageDaoImpl();
    private List<Message> inputMessageList;
    private List<Message> outputMessageList;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        try {
            User user = (User) session.getAttribute("userSession");
            inputMessageList = messageDao.getInputMessage(user.getId());
            outputMessageList = messageDao.getOutputMessage(user.getId());
            req.setAttribute("inputMessageList", inputMessageList);
            req.setAttribute("outputMessageList", outputMessageList);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/message.jsp");
            requestDispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if(req.getParameter("action").equals("sendMessage")) {
            String respondMessage = req.getParameter("respondMessage");
            String toUserId = req.getParameter("toUserId");
            String messageFromReq = req.getParameter("message");
            User user = (User) session.getAttribute("userSession");;
            UsersRolesDaoImpl usersRoles = new UsersRolesDaoImpl();
            List<Long> listUserAdmin;
            try {
                Message message = new Message();
                if (toUserId == null) {
                    listUserAdmin = usersRoles.getAdminId();
                    for (Long userAdminId : listUserAdmin) {
                        message.setMessage(messageFromReq);
                        message.setToUserId(userAdminId);
                        message.setFromUserId(user.getId());
                        message.setTime(new Timestamp( new java.util.Date().getTime()));
                        messageDao.save(message);
                    }
                }
                if (toUserId != null) {
                    message.setMessage(respondMessage);
                    message.setToUserId(Long.parseLong(toUserId));
                    message.setFromUserId(user.getId());
                    message.setTime(new Timestamp( new java.util.Date().getTime()));
                    messageDao.save(message);
                }
                inputMessageList = messageDao.getInputMessage(user.getId());
                outputMessageList = messageDao.getOutputMessage(user.getId());
                req.setAttribute("inputMessageList", inputMessageList);
                req.setAttribute("outputMessageList", outputMessageList);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/message.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (req.getParameter("action").equals("deleteMessage")) {
            long messageId = Long.parseLong(req.getParameter("deleteMessage"));
            try {
                messageDao.delete(messageId);
                User user = (User) session.getAttribute("userSession");
                inputMessageList = messageDao.getInputMessage(user.getId());
                outputMessageList = messageDao.getOutputMessage(user.getId());
                req.setAttribute("inputMessageList", inputMessageList);
                req.setAttribute("outputMessageList", outputMessageList);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/message.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
