package service;

import repository.MessageDaoImpl;
import entity.Message;
import entity.User;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

public class InitializationMessageServlet extends HttpServlet {

    public InitializationMessageServlet(HttpServletRequest req, HttpServletResponse resp) throws SQLException {

        List<Message> inputMessageList;
        List<Message> outputMessageList;
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("userSession");
        long userId = user.getId();
        MessageDaoImpl messageDao = new MessageDaoImpl();
        inputMessageList = messageDao.getInputMessage(userId);
        outputMessageList = messageDao.getOutputMessage(userId);
        req.setAttribute("inputMessageList", inputMessageList);
        req.setAttribute("outputMessageList", outputMessageList);
    }
}
