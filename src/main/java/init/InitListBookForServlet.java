package init;

import entity.Book;
import repository.BookDaoImpl;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class InitListBookForServlet {
    public InitListBookForServlet (HttpServletRequest req, HttpServletResponse resp) throws SQLException, ServletException, IOException {

        BookDaoImpl bookDao = new BookDaoImpl();
        List<Book> listBooks = bookDao.getAll();
        req.setAttribute("listBooks", listBooks); //Set book to request
//        RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("SessionServlet");
//        requestDispatcherSession.include(req, resp);
        HttpSession session = req.getSession();
        session.setAttribute("bookListSession", listBooks); //Set book to Session
    }
}
