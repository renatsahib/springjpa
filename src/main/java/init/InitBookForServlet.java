package init;

import repository.BookAuthorDaoImpl;
import repository.BookDaoImpl;
import repository.BookGenreDaoImpl;
import entity.Author;
import entity.Book;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

public class InitBookForServlet {
    public InitBookForServlet (long bookId, HttpServletRequest req, HttpServletResponse resp) throws SQLException{
        BookDaoImpl bookDao = new BookDaoImpl();
        Book book = bookDao.getById(bookId);
        HttpSession session = req.getSession();
        session.setAttribute("bookSession", book); //Set book to Session
    }
}
