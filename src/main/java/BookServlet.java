import entity.*;
import init.InitBookForServlet;
import repository.BookDaoImpl;
import repository.BucketDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static entity.UserRole.ADMIN;

@WebServlet(name = "BookServlet", urlPatterns = "/BookServlet", loadOnStartup = 0)
public class BookServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        try {
            long id = Long.parseLong(req.getParameter("bookId"));
            new InitBookForServlet(id,req,resp);
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("userSession");
            if (user != null){
                if (user.getUserRole().equals(ADMIN)) {
                    BucketDaoImpl bucketDao = new BucketDaoImpl();
                    session.setAttribute("bucketByBookIdSession", bucketDao.getBucketByBookId(id));
                }
            }
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book.jsp");
            requestDispatcher.forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        try {
            long bookId = (long) req.getAttribute("bookId");
            BookDaoImpl bookDao = new BookDaoImpl();
            Book book = bookDao.getById(bookId);
            HttpSession session = req.getSession();
            session.setAttribute("bookSession", book); //Set book to Session
            User user = (User) session.getAttribute("userSession");
            if (user != null){
                if (user.getUserRole().equals(ADMIN)) {
                    BucketDaoImpl bucketDao = new BucketDaoImpl();
                    session.setAttribute("bucketByBookIdSession", bucketDao.getBucketByBookId(bookId));
                }
            }
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book.jsp");
            requestDispatcher.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
