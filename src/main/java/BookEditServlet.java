import entity.*;
import init.InitBookForServlet;
import org.h2.util.IOUtils;
import repository.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "BookEditServlet", urlPatterns = "/BookEditServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class BookEditServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        BookGenreDaoImpl bookGenreDao = new BookGenreDaoImpl();
        AuthorDaoImpl authorDao = new AuthorDaoImpl();
        BookDaoImpl bookDao = new BookDaoImpl();
        BookAuthorDaoImpl bookAuthorDao = new BookAuthorDaoImpl();
        BookImageDaoImpl bookImageDao = new BookImageDaoImpl();

        Book book = new Book();

        //go to book edit
        if(req.getParameter("action").equals("goToEditBook")) {
            try {
                long bookId = Long.parseLong(req.getParameter("bookIdEditBook"));
                book = bookDao.getById(bookId);
                HttpSession session = req.getSession();
                session.setAttribute("bookSession", book); //Set book to Session
                req.setAttribute("authorsList", authorDao.getAll());
                req.setAttribute("genreList", bookGenreDao.getAll());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book_edit.jsp");
                requestDispatcher.forward(req,resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //go to book add
        if(req.getParameter("action").equals("goToAddBook")) {
            try {
                req.setAttribute("authorsList", authorDao.getAll());
                req.setAttribute("genreList", bookGenreDao.getAll());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book_add.jsp");
                requestDispatcher.forward(req,resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Add book
        if (req.getParameter("action").equals("addBook")) {
            String title = req.getParameter("new_Title");
            String year = req.getParameter("new_Year");
            String count = req.getParameter("new_Count");
            String genre = req.getParameter("new_Genre");
            String[] authors = req.getParameterValues("new_Authors");
            InputStream imageInputStream = req.getPart("new_Picture").getInputStream();
            byte[] image = IOUtils.readBytesAndClose(imageInputStream, imageInputStream.available());
            try {
                List<Book> bookList = bookDao.getAll();

                if (title == null || title.isEmpty()) {
                    throw new RuntimeException("Please enter Book Title!");
                }

                if (year == null || year.isEmpty()) {
                    throw new RuntimeException("Please enter Book Year!");
                }

                if ( count == null || count.isEmpty()) {
                    throw new RuntimeException("Please enter Count!");
                }

                if ( genre == null || genre.isEmpty()) {
                    throw new RuntimeException("Please enter Genre!");
                }

                if ( authors == null) {
                    throw new RuntimeException("Please enter Authors!");
                }

                for (Book books: bookList){
                    if (title.equals(books.getTittle())) {
                        throw new RuntimeException("This title exist!");
                    }
                }
                book.setTittle(title);
                book.setYear(Integer.parseInt(year));
                book.setCount(Integer.parseInt(count));
                book.setBookGenre(bookGenreDao.getById(Long.parseLong(genre)));
                bookDao.save(book);
                List<Long> authorId = new ArrayList<>();
                for (String author:authors){
                    if (!author.equals("0")){
                        bookAuthorDao.setBookAuthors(book.getId(), Long.parseLong(author));
                        authorId.add(Long.parseLong(author));
                    }
                }
                if(image.length > 1) {
                    bookImageDao.addImage(book.getId(), image);
                }

                req.setAttribute("bookId", book.getId());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/BookServlet");
//                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/IndexServlet");
                requestDispatcher.forward(req,resp);

            } catch (RuntimeException e) {
                req.setAttribute("status_addBook", e.getMessage());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book_add.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //delete Book
        if(req.getParameter("action").equals("deleteBook")) {
            long bookId = Long.parseLong(req.getParameter("bookIdDeleteBook"));
            try {
                BucketDaoImpl bucketDao = new BucketDaoImpl();
                bookImageDao.deleteImage(bookId);
                bookAuthorDao.removeBookAuthor(bookId);
                bucketDao.deleteBucketByBookId(bookId);
                bookDao.delete(bookId);
                List<Book> listBookDao = bookDao.getAll();
                HttpSession session = req.getSession();
                session.setAttribute("bookListSession", listBookDao); //Set bookList to Session
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/index.jsp");
                requestDispatcher.forward(req, resp);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Edit book
        if (req.getParameter("action").equals("editBook")) {
            long bookId = Long.parseLong(req.getParameter("bookId"));
            String title = req.getParameter("new_Title");
            String year = req.getParameter("new_Year");
            String count = req.getParameter("new_Count");
            String genre = req.getParameter("new_Genre");
            String[] authors = req.getParameterValues("new_Authors");
            InputStream imageInputStream = req.getPart("new_Picture").getInputStream();
            byte[] image = IOUtils.readBytesAndClose(imageInputStream, imageInputStream.available());
            try {
                if (title == null || title.isEmpty()) {
                    throw new RuntimeException("Please enter Book Title!");
                }

                if (year == null || year.isEmpty()) {
                    throw new RuntimeException("Please enter Book Year!");
                }

                if ( count == null || count.isEmpty()) {
                    throw new RuntimeException("Please enter Count!");
                }

                if ( genre == null || genre.isEmpty()) {
                    throw new RuntimeException("Please enter Genre!");
                }

                if ( authors == null) {
                    throw new RuntimeException("Please enter Authors!");
                }
                book = bookDao.getById(bookId);
                book.setTittle(title);
                book.setYear(Integer.parseInt(year));
                book.setCount(Integer.parseInt(count));
                book.setBookGenre(bookGenreDao.getById(Long.parseLong(genre)));
                bookDao.update(book);
                List<Long> authorId = new ArrayList<>();
                for (String author:authors){
                    if (!author.equals("0")){
                        authorId.add(Long.parseLong(author));
                    }
                }
                bookAuthorDao.updateBookAuthor(book.getId(), authorId);
                if(image.length > 1) {
                    bookImageDao.addImage(book.getId(), image);
                }
                req.setAttribute("bookId", book.getId());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/BookServlet");
                requestDispatcher.forward(req,resp);

            } catch (RuntimeException e) {
                req.setAttribute("status_editBook", e.getMessage());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/book_edit.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
