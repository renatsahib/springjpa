package dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

public interface BookImageDao {

    void addImage(long bookId, InputStream image) throws SQLException;
    byte[] getImage(long bookId) throws SQLException;
    byte[] getNoneImage() throws SQLException;
    void deleteImage(long bookId) throws SQLException;
}
