package dao;

import java.sql.SQLException;

public interface UserDao {

    long getUserId() throws SQLException;

}
