package dao;

import entity.BlockedTime;
import entity.User;
import repository.BlockedTimeDaoImpl;
import repository.UsersRolesDaoImpl;
import service.DataBaseConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends AbstractCrudDao<User> implements CrudDao<User>, UserDao{

    @Override
    protected String getByIdSql() {return "SELECT * FROM USERS WHERE Id = ?";}

    @Override
    protected String getAllSql() {return "SELECT * FROM USERS";}

    @Override
    protected String updateSql() {return "UPDATE USERS SET NAME = ?, SURNAME = ?, AGE = ?, EMAIL = ? WHERE ID = ?";}

    @Override
    protected String createSql() {return "INSERT INTO USERS (NAME, SURNAME, AGE, EMAIL) VALUES (?,?,?,?)";}

    @Override
    protected String deleteSql() {return "DELETE FROM USERS WHERE Id = ?";}

    private String getUserIdSql() {return "SELECT MAX(`Id`) FROM `USERS`";}

    @Override
    protected User resultSetMapperById(PreparedStatement stmt) throws SQLException {

        User user = new User();
        UsersRolesDaoImpl role = new UsersRolesDaoImpl();
        ResultSet resultSet = stmt.getResultSet();

        while (resultSet.next()) {
            user.setId(resultSet.getInt("Id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            user.setAge(resultSet.getInt("age"));
            user.setEmail(resultSet.getString("email"));

//            user.setRole(role.getUserRole(user.getId()));

        }
        return user;
    }

    @Override
    protected List<User> resultSetMapperGetAll(PreparedStatement stmt) throws SQLException {

            List<User> userList = new ArrayList<>();
            ResultSet resultSet = stmt.getResultSet();
            while (resultSet.next()){
                User user = new User();
                UsersRolesDaoImpl role = new UsersRolesDaoImpl();
                BlockedTimeDaoImpl blockedTimeDao = new BlockedTimeDaoImpl();

                user.setId(resultSet.getInt("Id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setAge(resultSet.getInt("age"));
                user.setEmail(resultSet.getString("email"));

                BlockedTime blockedTime = blockedTimeDao.getById(user.getId());
//                user.setRole(role.getUserRole(user.getId()));
//                user.setBlockedTime(blockedTime.getBlockedTime());
                userList.add(user);
            }
            return userList;
    }

    @Override
    protected User resultSetMapperCreate(PreparedStatement stmt, User o) throws SQLException {

        stmt.setString(1,o.getName());
        stmt.setString(2,o.getSurname());
        stmt.setInt(3,o.getAge());
        stmt.setString(4,o.getEmail());

        stmt.executeUpdate();

        return o;
    }

    @Override
    protected User resultSetMapperUpdate(PreparedStatement stmt, User o) throws SQLException {

        stmt.setString(1,o.getName());
        stmt.setString(2,o.getSurname());
        stmt.setInt(3,o.getAge());
        stmt.setString(4,o.getEmail());
        stmt.setInt(5, (int) o.getId());

        stmt.executeUpdate();

        return o;
    }

    @Override
    public void resultSetMapperDelete(PreparedStatement stmt) throws SQLException {

            stmt.executeUpdate();
    }


    @Override
    public long getUserId() throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getUserIdSql())) {

            stmt.executeQuery();
            ResultSet resultSet = stmt.getResultSet();
            long userId = -1;
            while (resultSet.next()) {
                userId = resultSet.getLong("MAX(Id)");
            }
            return userId;

        }
    }
}

