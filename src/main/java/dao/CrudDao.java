package dao;

import java.sql.SQLException;
import java.util.List;

public interface CrudDao <T> {

    T getById(long id) throws SQLException;

    List<T> getAll() throws SQLException;

    T create(T t) throws SQLException;

    T update(T t) throws SQLException;

    void delete(long id) throws SQLException;

}
