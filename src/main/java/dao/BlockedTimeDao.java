package dao;

import entity.BlockedTime;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface BlockedTimeDao {

    void addBlockedTime(long userId, Date blockedTime) throws SQLException;
    void removeBlockedTime(long userId) throws SQLException;
    BlockedTime getBlockedTime(long userId) throws SQLException;
    List<BlockedTime> getAllBlockedTime() throws SQLException;

}
