package dao;

import entity.BlockedTime;
import entity.Bucket;
import service.DataBaseConnector;

import javax.persistence.EntityManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static util.JpaEntityManagerFactoryUtil.getEntityManager;

public class BlockedTimeDaoImpl implements BlockedTimeDao{

    private String getBlockedTimeSql() {return "SELECT * FROM BLOCKED_TIME where USER_ID = ?";}
    private String getAllBlockedTimeSql() {return "SELECT * FROM BLOCKED_TIME";}
    private String addBlockedTimeSql() {return "INSERT INTO BLOCKED_TIME (USER_ID, Blocked_Time) VALUES (?,?)";}
    private String removeBlockedTimeSql() {return "DELETE FROM BLOCKED_TIME WHERE USER_ID = ?";}

    @Override
    public void addBlockedTime(long userId, Date blockedTime) throws SQLException {

        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(addBlockedTimeSql())) {

            stmt.setLong(1, userId);
            stmt.setDate(2, blockedTime);
            stmt.executeUpdate();
        }

    }

    @Override
    public void removeBlockedTime(long userId) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM BlockedTime WHERE BlockedTime.userId= ?1")
                .setParameter(1, userId)
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public BlockedTime getBlockedTime(long userId) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getBlockedTimeSql())) {

            stmt.setLong(1, userId);
            stmt.executeQuery();

            ResultSet resultSet = stmt.getResultSet();
            BlockedTime blockedTime = new BlockedTime();
            Date nowDate = new Date(new java.util.Date().getTime());
            while (resultSet.next()) {
                blockedTime.setId(resultSet.getLong("Id"));
                blockedTime.setUserId(resultSet.getLong("User_id"));
                if (nowDate.compareTo(resultSet.getDate("Blocked_Time")) > 0) {
                    removeBlockedTime(userId);
                    blockedTime.setBlockedTime(null);
                } else {
                    blockedTime.setBlockedTime(resultSet.getDate("Blocked_Time"));
                }
            }

            return blockedTime;
        }
    }

    @Override
    public List<BlockedTime> getAllBlockedTime() throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getAllBlockedTimeSql())) {

            stmt.executeQuery();

            ResultSet resultSet = stmt.getResultSet();
            List<BlockedTime> listBlockedTime = new ArrayList<>();
            BlockedTime blockedTime = new BlockedTime();
            while (resultSet.next()) {
                blockedTime.setId(resultSet.getLong("Id"));
                blockedTime.setUserId(resultSet.getLong("User_id"));
                blockedTime.setBlockedTime(resultSet.getDate("Blocked_Time"));
                listBlockedTime.add(blockedTime);
            }
            return listBlockedTime;
        }
    }
}
