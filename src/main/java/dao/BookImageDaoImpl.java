package dao;

import entity.Book;
import service.DataBaseConnector;

import java.io.InputStream;
import java.sql.*;

public class BookImageDaoImpl implements BookImageDao{

    private String addImageSql() {return "INSERT INTO BOOKS_IMAGES (book_id, image) VALUES (?,?)";}
    private String getImageSql() {return "SELECT image FROM BOOKS_IMAGES where book_id = ?";}
    private String getNoneImageSql() {return "SELECT image FROM BOOKS_IMAGES where id = 1";}

    @Override
    public void addImage(long bookId, InputStream image) throws SQLException{
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(addImageSql())) {

            stmt.setLong(1, bookId);
            stmt.setBlob(2, image);
            stmt.executeUpdate();
        }
    }

    @Override
    public byte[] getImage(long bookId) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getImageSql())) {

            stmt.setLong(1, bookId);
            stmt.executeQuery();

            ResultSet resultSet = stmt.getResultSet();
            byte[] image = null;
            while (resultSet.next()) {
                image = resultSet.getBytes("image");
            }
            if (image == null) {
                image = getNoneImage();
            }
            return image;
        }
    }

    @Override
    public byte[] getNoneImage() throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getNoneImageSql())) {

            stmt.executeQuery();

            ResultSet resultSet = stmt.getResultSet();
            byte[] image = null;
            while (resultSet.next()) {
                image = resultSet.getBytes("image");
            }
            return image;
        }
    }

    @Override
    public void deleteImage(long bookId) {

    }


}
