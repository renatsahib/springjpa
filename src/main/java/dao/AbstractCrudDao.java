package dao;

import service.DataBaseConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;


public abstract class AbstractCrudDao<T> implements CrudDao<T> {

    protected abstract String getByIdSql();
    protected abstract T resultSetMapperById(PreparedStatement stmt) throws SQLException;
    protected abstract String getAllSql();
    protected abstract List<T> resultSetMapperGetAll(PreparedStatement stmt) throws SQLException;
    protected abstract String updateSql();
    protected abstract T resultSetMapperUpdate(PreparedStatement stmt, T t) throws SQLException;
    protected abstract String createSql();
    protected abstract T resultSetMapperCreate(PreparedStatement stmt, T t) throws SQLException;
    protected abstract String deleteSql();
    protected abstract void resultSetMapperDelete(PreparedStatement stmt) throws  SQLException;


    @Override
    public T getById(long id) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getByIdSql())) {

            stmt.setString(1, String.valueOf(id));
            stmt.executeQuery();

            return resultSetMapperById(stmt);
        }
    }

    @Override
    public List<T> getAll() throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(getAllSql())) {

            stmt.executeQuery();
            return resultSetMapperGetAll(stmt);
        }
    }

    public T update(T t) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(updateSql())) {

            T o = (T) t;
            return resultSetMapperUpdate(stmt, o);
        }
    }

    public T create(T t) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(createSql())) {

            T o = (T) t;
            return resultSetMapperCreate(stmt, o);
        }
    }

    public void delete(long id) throws SQLException {
        try (Connection connection = DataBaseConnector.getInstance().getConnection();
             PreparedStatement stmt = connection.prepareStatement(deleteSql())) {

            stmt.setString(1, String.valueOf(id));
            stmt.executeUpdate();
        }
    }
}