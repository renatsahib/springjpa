import entity.Authenticate;
import entity.User;
import repository.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "EditProfileServlet", urlPatterns = "/EditProfileServlet")
public class EditProfileServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/edit_user_profile.jsp");
        requestDispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        //Edit profile
        if (req.getParameter("action").equals("EditProfile")) {
            String oldPassword = req.getParameter("old_password");
            String editPassword = req.getParameter("new_password");
            String editName = req.getParameter("new_name");
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("userSession");
            UserDaoImpl userDao = new UserDaoImpl();
            Authenticate authenticate = user.getAuthenticate();
            try {
                if (editPassword == null || editPassword.isEmpty()) {
                    throw new RuntimeException("Please enter PASSWORD!");
                }

                if (editName == null || editName.isEmpty()) {
                    throw new RuntimeException("Please enter NAME!");
                }

                if (oldPassword == null || oldPassword.isEmpty()) {
                    throw new RuntimeException("Please enter Old Password!");
                }

                if (!oldPassword.equals(authenticate.getPassword())) {
                    throw new RuntimeException("false Old Password!");
                }
                if (oldPassword.equals(authenticate.getPassword())) {
                    if (!editPassword.isEmpty() && !editName.isEmpty()) {
                        user.setName(editName);
                        user.setSurname(req.getParameter("new_second_name"));
                        user.setAge(Integer.parseInt(req.getParameter("new_age")));
                        authenticate.setPassword(req.getParameter("new_password"));
                        userDao.update(user);
                        req.setAttribute("user", user);
                        RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                        requestDispatcherSession.include(req, resp);

                        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                        requestDispatcher.forward(req,resp);
                    }
                }
            } catch (RuntimeException e) {
                req.setAttribute("status_registration", e.getMessage());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/edit_user_profile.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
