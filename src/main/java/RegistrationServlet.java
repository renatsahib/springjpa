import entity.*;
import repository.*;
import repository.UserDaoImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static entity.UserRole.ADMIN;
import static entity.UserRole.USER;


@WebServlet (name = "RegistrationServlet", urlPatterns = "/RegistrationServlet", loadOnStartup = 0)
public class RegistrationServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        UserDaoImpl userDao = new UserDaoImpl();
        UsersRolesDaoImpl userRole = new UsersRolesDaoImpl();
        BucketDaoImpl bucketDao = new BucketDaoImpl();
        AuthenticateDaoImpl authenticateDao = new AuthenticateDaoImpl();
        long sessionUserId = -1;

        //Login
        if (req.getParameter("action").equals("Login")) {

            String login = req.getParameter("login");
            String password = req.getParameter("password");

            try {
                if (login == null || login.isEmpty()) {
                    throw new RuntimeException("Please enter Login!");
                } else {
                    if (password == null || password.isEmpty()){
                        throw new RuntimeException("Please enter Password!");
                    } else {
                        try {
                            sessionUserId = authenticateDao.checkLogin(login); //get user_id for logging user
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

            } catch (RuntimeException e) {
                req.setAttribute("error", e.getMessage());
            }

            //Password check, if true, set to request entity.User
            try {
                if (sessionUserId != -1) {
                    if (authenticateDao.getById(sessionUserId).getPassword().equals(password)) {
                        User loginedUser = userDao.getById(sessionUserId);
//                        BlockedTimeDaoImpl blockedTimeDao = new BlockedTimeDaoImpl();
//                        BlockedTime blockedTime;
//                        blockedTime = blockedTimeDao.getBlockedTime(loginedUser.getId());
// !!!!!!!                       loginedUser.setBlockedTime(blockedTime.getBlockedTime()); //add blocked time to user

                        if (loginedUser.getBlockedTime() != null &&
                                new Date(new java.util.Date().getTime())
                                    .compareTo(loginedUser.getBlockedTime().getBlockedTime()) > 0) {
                            if (loginedUser.getAuthenticate().getProfileStatus().equals(ProfileStatus.TEMP_BlOCK)) {
                                ProfileStatusDaoImpl profileStatusDao = new ProfileStatusDaoImpl();
                                profileStatusDao.updateProfileStatus(loginedUser.getId(), ProfileStatus.ACTIV);

                                BlockedTimeDaoImpl blockedTimeDao = new BlockedTimeDaoImpl();
                                blockedTimeDao.removeBlockedTime(loginedUser.getId());
                            }
                        }

                        //Get User.List for all user if logging user admin
                        try {
                            if (loginedUser.getUserRole().equals(ADMIN)) { //userDao.getRole(sessionUserId)
                                req.setAttribute("userList", userDao.getAll());
                                req.setAttribute("userListBucket", bucketDao.getAllBucket());
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }

                        //get User Bucket
//                        if (loginedUser.getBucketSet().size() != 0) {
//                            BookDaoImpl bookDao = new BookDaoImpl();
//                            List<Book> listBook = new ArrayList<>();
//                            List<Bucket> listBucket = bucketDao.getBucket(loginedUser.getId());
//                            for(Bucket bucket: listBucket){
//                                Book book;
////                                book = bookDao.getById(bucket.getBookID());
//                                book = bookDao.getById(bucket.getBook().getId());
//                                listBook.add(book);
//                            }
////                            Book book = bookDao.getById(bucket.getId());
//                            req.setAttribute("book", listBook);
//                            req.setAttribute("bucket", listBucket);
//                        }
//                        ProfileStatusDaoImpl profileStatusDao = new ProfileStatusDaoImpl();
//                        req.setAttribute("userStatus", loginedUser.getAuthenticate().getProfileStatus());
                        req.setAttribute("user", loginedUser);

                        RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                        requestDispatcherSession.include(req, resp);

                    } else {
                        throw new RuntimeException("Wrong password.");
                    }
                } else {
                    throw  new RuntimeException("Not found user or wrong password.");
                }

                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req, resp);

            } catch (RuntimeException e) {
                req.setAttribute("error", e.getMessage());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/login.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //Registration
        if (req.getParameter("action").equals("Registration")) {
            String newLogin = req.getParameter("new_login");
            String newPassword = req.getParameter("new_password");
            String newName = req.getParameter("new_name");
            long userId = -1;

            try {
                if (newLogin == null || newLogin.isEmpty()) {
                    throw new RuntimeException("Please enter LOGIN!");
                } else {
                    userId = authenticateDao.checkLogin(newLogin);
                    if (userId > 0) {
                        throw new RuntimeException("This login is already exist.");
                    }
                }

                if (newPassword == null || newPassword.isEmpty()) {
                    throw new RuntimeException("Please enter PASSWORD!");
                }

                if (newName == null || newName.isEmpty()) {
                    throw new RuntimeException("Please enter NAME!");
                }

                if (userId == -1 && !newPassword.isEmpty() && !newName.isEmpty()) {

                    User newUser = new User(req.getParameter("new_name"), req.getParameter("new_second_name"), Integer.parseInt(req.getParameter("new_age")), req.getParameter("new_email"));

                    //set role for new User
                    if(req.getParameter("admin") != null){
                        newUser.setUserRole(ADMIN);
                    } else {
                        newUser.setUserRole(USER);
                    }

//                    userId = userDao.getUserId(); //get last user_id in DB
                    Authenticate authenticate = new Authenticate(req.getParameter("new_login"), req.getParameter("new_password"), ProfileStatus.ACTIV);
                    authenticate.setUser(newUser);
                    newUser.setAuthenticate(authenticate);
                    userDao.save(newUser);

                    //get updated entity for admin panel if role = admin
                    if ((req.getSession().getAttribute("userSession") != null)) {
                        User loginedUser = (User) req.getSession().getAttribute("userSession");
                        if (loginedUser.getUserRole() == ADMIN) {
                                req.setAttribute("userList", userDao.getAll());
//                                req.setAttribute("userListAuthenticate", authenticateDao.getAll());
                                req.setAttribute("userListBucket", bucketDao.getAllBucket());

                                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                                requestDispatcherSession.include(req, resp);

                                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                                requestDispatcher.forward(req,resp);
                                return;
                            }
                    }

                    req.setAttribute("user", newUser);

                    if(req.getSession().getAttribute("userSession") == null) {
                        RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                        requestDispatcherSession.include(req, resp);

                        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                        requestDispatcher.forward(req,resp);
                        return;
                    } else {
                        RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                        requestDispatcherSession.include(req, resp);

                        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/index.jsp");
                        requestDispatcher.forward(req,resp);
                        return;
                    }
                }

            } catch (RuntimeException e) {
                req.setAttribute("status_registration", e.getMessage());
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/registration.jsp");
                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Delete User
        if(req.getParameter("action").equals("deleteUser")) {

            String userId = req.getParameter("loginDeleteUser");
            long idDeleteUser = Long.parseLong(userId);

            try {
                userDao.delete(idDeleteUser); //remove user from DB

                req.setAttribute("userList", userDao.getAll()); //set to request new user ArrayList
//                req.setAttribute("userListAuthenticate", authenticateDao.getAll()); //set to request new userAuthenticate ArrayList

                RequestDispatcher requestDispatcherSession = req.getRequestDispatcher("/SessionServlet");
                requestDispatcherSession.include(req, resp);

                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/user_panel.jsp");
                requestDispatcher.forward(req,resp);
                return;
//                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/index.jsp");
//                requestDispatcher.forward(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //Add newUser
        if(req.getParameter("action").equals("addUser")) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/registration.jsp");
            requestDispatcher.forward(req, resp);
            return;
        }
    }
}
