import repository.BucketDaoImpl;
import dao.UserDaoImpl;
import entity.Bucket;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "DebtorsServlet", urlPatterns = "/DebtorsServlet", loadOnStartup = 0)
public class DebtorsServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Date nowDate = new Date(new java.util.Date().getTime());
        Date reservedDate;
        List<Bucket> debtors = new ArrayList<>();
        BucketDaoImpl bucketDao = new BucketDaoImpl();
        Bucket bucketUser;
        try {
            List<Bucket> bucketList = bucketDao.getAllBucket();
            for(Bucket bucket: bucketList) {
                reservedDate = bucket.getReservedDays();
                if (nowDate.compareTo(reservedDate) > 0) {
                    bucketUser = bucketDao.getBucketById(bucket.getId());
                    debtors.add(bucketUser);
                }
            }

            req.setAttribute("debtors", debtors);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/pages/debtors.jsp");
            requestDispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
