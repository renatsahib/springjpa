<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library - User panel.</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body>
<c:set var="user" value="${sessionScope.userSession}" />
    <div class="admin-panel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-tittle">Hi <c:out value="${user.authenticate.login}"/>!</h3>
                <c:if test="${user.blockedTime.blockedTime != null}">
                    <h3 style="color: red">Your Blocked Time: <c:out value="${user.blockedTime.blockedTime}"></c:out></h3>
                    <a href="/MessageServlet?userId=${user.id}">Send Message.</a>
                </c:if>
            </div>
            <ul class="list-group">
                <li class="list-group-item">Name: ${user.name}</li>
                <li class="list-group-item">Second Name: ${user.surname}</li>
                <li class="list-group-item">Email: ${user.email}</li>
                <li class="list-group-item">Age: ${user.age}</li>
                <li class="list-group-item">Role: ${user.userRole}</li>
                <li class="list-group-item">Status: ${user.authenticate.profileStatus}</li>
                <li class="list-group-item"><a href="/EditProfileServlet?userId=${user.id}">Edit Profile</a> </li>
                <c:if test="${user.bucketSet['empty']}">
                    <li class="list-group-item">
                        Bucket empty.
                    </li>
                </c:if>
            </ul>
        </div>
        <c:if test="${!user.bucketSet['empty']}">
            <div class="panel panel-default">
                <h3 style="text-align: center">BUCKET</h3>
                <ul class="list-group">
                    <c:set var="userBuckets" value="${user.bucketSet}" />
                    <c:forEach var="bucket" items="${userBuckets}" >
                        <li class="list-group-item">Title: <a href="/BookServlet?bookId=${bucket.book.id}"><c:out value="${bucket.book.tittle}"></c:out></a></li>
                        <li class="list-group-item">Reserved: <c:out value="${bucket.reservedDays}"></c:out></li>
                        <form action="/BucketServlet" method="post">
                            <input type="hidden" id="bookId" name="bookId" value="${bucket.book.id}"/>
                            <button class="btn btn-default" type="submit" name="action" value="returnBook">Return Book</button>
                        </form>
                    </c:forEach>
                </ul>
            </div>
        </c:if>
    </div>


    <c:if test="${user != null}">
        <c:if test="${user.userRole == 'ADMIN'}">
        <div class="panel panel-default" style="width: 50%; margin: auto">
            <ul class="list-group">
                <li class="list-group-item"><a href="/DebtorsServlet">Debtors List</a> </li>
                <li class="list-group-item"><a href="/MessageServlet?userId=${user.id}">Send Message.</a></li>
                <div>
                    <form style="margin-bottom: 0px;" action="../BookEditServlet" method="post">
                        <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="goToAddBook">Add Book</button>
                    </form>
                </div>
            </ul>
        </div>
        <div class="panel panel-default" style="width: 90%; margin-top: 25px; margin-left: auto; margin-right: auto">
            <div class="panel panel-default">
                <div class="panel-heading">
                   <h4 class="panel-tittle">Users!</h4>
                </div>
            </div>
            <div class="panel panel-default">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Login</th>
                        <th>Name</th>
                        <th>Second Name</th>
                        <th>Age</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="userListSession" value="${sessionScope.userListSession}" />
                    <c:forEach var="userList" items="${userListSession}" >
                        <tr>
                            <td>
                                <a href="/UserProfileServlet?userId=${userList.id}">
                                    <c:out value="${userList.authenticate.login}"></c:out></a>
                            </td>
                            <td><c:out value="${userList.name}"></c:out></td>
                            <td><c:out value="${userList.surname}"></c:out></td>
                            <td><c:out value="${userList.age}"></c:out></td>
                            <td><c:out value="${userList.email}"></c:out></td>
                            <td><c:if test="${userList.userRole == 'ADMIN'}">admin</c:if>
                                <c:if test="${userList.userRole == 'USER'}">user</c:if>
                            </td>
                            <c:set var="profileStatus" value="${userList.authenticate.profileStatus}"/>
                            <td>
                                <c:if test="${profileStatus == 'ACTIV'}">ACTIV</c:if>
                                <c:if test="${profileStatus == 'TEMP_BlOCK'}">TEMP_BLOCK</c:if>
                                <c:if test="${profileStatus == 'BLOCK'}">BLOCK</c:if>
                            </td>
                            <td>
                                <c:if test="${user.authenticate.login != userList.authenticate.login}">
                                    <form style="margin-bottom: 0px;" action="../RegistrationServlet" method="post">
                                        <input type="hidden" name="loginDeleteUser" value="${userList.id}"/>
                                        <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="deleteUser">Delete User</button>
                                    </form>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${profileStatus != 'ACTIV'}">
                                    <form style="margin-bottom: 0px;" action="../ChangeProfileStatusServlet" method="post">
                                        <input type="hidden" name="unlockUser" value="${userList.id}"/>
                                        <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="unlockUser">Unlock User</button>
                                    </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <form action="../RegistrationServlet" method="post">
                <button class="btn btn-default" type="submit" name="action" value="addUser">Add User</button>
            </form>
            </div>
        </c:if>
    </c:if>
    <div class="panel panel-default" style="width: 60%;margin: auto">
        <form action="/LogOutServlet" method="post">
            <button class="btn btn-default" type="submit" name="logout" value="logout">Logout</button>
        </form>
        <h4 style="margin: 0;"><a href = "/IndexServlet">Main page.</a></h4>
    </div>
</body>
</html>
