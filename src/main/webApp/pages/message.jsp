<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library - user message.</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body>
<%--<c:set var="userList" value="${sessionScope.userListSession}" />--%>
<%--<c:set var="userListAuthenticate" value="${sessionScope.userListAuthenticateSession}" />--%>
<c:set var="inputMessageList" value="${requestScope.inputMessageList}" />
<c:set var="outputMessageList" value="${requestScope.outputMessageList}" />
<div class="admin-panel" style="max-width: max-content;">
    <div>
        <form action="../MessageServlet" method="post">
            <label for="message">Message</label>
            <input id="message" name="message" type="text" placeholder=""/>
            <button class="btn btn-default" type="submit" name="action" value="sendMessage">Send Message</button>
        </form>
    </div>
    <div class="panel panel-default">
        <table class="table">
            <thead>
                <tr>
                    <th>
                        <c:if test="${sessionScope.userSession.userRole == 'ADMIN'}">
                            From
                        </c:if>
                    </th>
                    <th>Input Message</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="inputMessage" items="${inputMessageList}">
                    <tr>
                        <td>
                            <c:if test="${sessionScope.userSession.userRole == 'ADMIN'}">
                                <c:forEach var="user" items="${sessionScope.userListSession}"  >
                                    <c:if test="${user.id == inputMessage.fromUserId}">
                                        <a href="/UserProfileServlet?userId=${user.id}">${user.authenticate.login}</a>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </td>
                        <td><c:out value="${inputMessage.message}"/></td>
                        <td><c:out value="${inputMessage.time}"/></td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <form style="margin-bottom: 0px; padding-top: 10px;" action="../MessageServlet" method="post">
                                <label for="respondMessage">Message</label>
                                <input id="respondMessage" name="respondMessage" type="text" placeholder=""/>
                                <input type="hidden" name="toUserId" value="${inputMessage.fromUserId}"/>
                                <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="sendMessage">Respond</button>
                            </form>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="panel panel-default">
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th>Output Message</th>
                <th>Time</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="outputMessage" items="${outputMessageList}">
                    <tr>
                        <td></td>
                        <td><c:out value="${outputMessage.message}"/></td>
                        <td><c:out value="${outputMessage.time}"/></td>
                        <td>
                            <form style="margin-bottom: 0px; padding-top: 10px;" action="../MessageServlet" method="post">
                                <input type="hidden" name="deleteMessage" value="${outputMessage.id}"/>
                                <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="deleteMessage">Delete</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="admin-panel">
        <h4 style="margin: 0;"><a href = "/IndexServlet">Main page.</a></h4>
    </div>
</div>