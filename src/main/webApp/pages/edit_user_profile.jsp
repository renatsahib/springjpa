<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library - edit User profile</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body style="margin: auto;width: 600px;">
<h2>Edit Form</h2>
<div class="container" style="width: 600px">
    <%--       Set sessionScope.userSession to user      --%>
    <c:set var="user" value="${sessionScope.userSession}" />
    <form action="../EditProfileServlet" method="post">
        <div class="panel panel-default">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><label>Login:</label></td>
                    <td><c:out value="${user.authenticate.login}"/></td>
                </tr>
                <tr>
                    <td><label for="old_password">Old Password</label></td>
                    <td><input id="old_password" name="old_password" type="text" placeholder="Enter old password"/></td>
                </tr>
                <tr>
                    <td><label for="new_password">New Password</label></td>
                    <td><input id="new_password" name="new_password" type="text" placeholder="Enter password"/></td>
                </tr>
                <tr>
                    <td><label for="new_name">Name</label></td>
                    <td><input id="new_name" name="new_name" type="text" value="${user.name}" placeholder="${user.name}"/></td>
                </tr>
                <tr>
                    <td><label for="new_second_name">Second Name</label></td>
                    <td><input id="new_second_name" name="new_second_name" type="text" value="${user.surname}" placeholder="${user.surname}"/></td>
                </tr>
                <tr>
                    <td><label>Email:</label></td>
                    <td><c:out value="${user.email}"/></td>
                </tr>
                <tr>
                    <td><label for="new_age">Age</label></td>
                    <td><input id="new_age" name="new_age" type="number" min="5" max="100" value="${user.age}" placeholder="${user.age}"/></td>
                </tr>
                <c:if test="${user.userRole == 'ADMIN'}">
                    <tr>
                        <td><label for="admin">Set user admin</label>
                            <input type="checkbox" id="admin" name="admin" value="admin"/></td>
                    </tr>
                </c:if>
                </tbody>
            </table>
        </div>
        <c:if test="${requestScope.status_registration != null}">
            <div class="alert alert-danger" style="margin: 0">${requestScope.status_registration}</div>
        </c:if>
        <button class="btn btn-default" type="submit" name="action" value="EditProfile">Save Profile</button>
    </form>
</div>
</body>
</html>
