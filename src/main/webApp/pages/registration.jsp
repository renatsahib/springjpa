<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library Registration.</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body style="margin: auto;width: 600px;">
    <h2>Registration Form</h2>
<div class="container" style="width: 600px">
    <form action="../RegistrationServlet" method="post">
        <div class="panel panel-default">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><label for="new_login">Login</label></td>
                        <td><input id="new_login" name="new_login" type="text" placeholder="Enter Login"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_password">Password</label></td>
                        <td><input id="new_password" name="new_password" type="text" placeholder="Enter password"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_name">Name</label></td>
                        <td><input id="new_name" name="new_name" type="text" placeholder="Your Name"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_second_name">Second Name</label></td>
                        <td><input id="new_second_name" name="new_second_name" type="text" placeholder="Your Second Name"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_email">Email</label></td>
                        <td><input id="new_email" name="new_email" type="email" placeholder="Your Email"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_age">Age</label></td>
                        <td><input id="new_age" name="new_age" type="number" min="5" max="100" placeholder="Your Age"/></td>
                    </tr>
                    <c:if test="${sessionScope.userSession.userRole == 'ADMIN'}">
                        <tr>
                            <td><label for="admin">Set user admin</label>
                            <input type="checkbox" id="admin" name="admin" value="admin"/></td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
        </div>
        <c:if test="${requestScope.status_registration != null}">
            <div class="alert alert-danger" style="margin: 0">${requestScope.status_registration}</div>
        </c:if>
        <button class="btn btn-default" type="submit" name="action" value="Registration">Registration</button>
    </form>
</div>
</body>
</html>
