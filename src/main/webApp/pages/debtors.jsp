<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library - debtors</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body>
<div class="admin-panel">
    <div class="panel panel-default">
        <c:set var="debtors" value="${requestScope.debtors}" />
        <c:forEach var="bucketDebtors" items="${debtors}">
            <ul class="list-group">
                <li class="list-group-item">Login: <a href="/UserProfileServlet?userId=${bucketDebtors.user.id}"><c:out value="${bucketDebtors.user.authenticate.login}"></c:out></a></li>
                <li class="list-group-item">Status:
                    <c:if test="${bucketDebtors.user.authenticate.profileStatus == 'ACTIV'}">ACTIV</c:if>
                    <c:if test="${bucketDebtors.user.authenticate.profileStatus == 'TEMP_BlOCK'}">TEMP_BLOCK</c:if>
                    <c:if test="${bucketDebtors.user.authenticate.profileStatus == 'BLOCK'}">BLOCK</c:if>
                </li>
                <c:if test="${bucketDebtors.user.blockedTime.blockedTime != null}">
                    <li class="list-group-item">Blocked Time: <c:out value="${bucketDebtors.user.blockedTime.blockedTime}"></c:out></li>
                </c:if>
                <li class="list-group-item">Name: ${bucketDebtors.user.name}</li>
            </ul>
            <ul class="list-group">
                <li class="list-group-item">Title: <a href="/BookServlet?bookId=${bucketDebtors.book.id}"><c:out value="${bucketDebtors.book.tittle}"></c:out></a></li>
                <li class="list-group-item">Reserved: <c:out value="${bucketDebtors.reservedDays}"></c:out>
                        <%--<form action="/BucketServlet" method="post">--%>
                        <%--<input type="hidden" id="bookId" name="bookId" value="${book.id}"/>--%>
                        <%--<button class="btn btn-default" type="submit" name="action" value="returnBook">Return Book</button>--%>
                        <%--</form>--%>
                </li>
            </ul>
            <ul class="list-group">
                <li class="list-group-item">
                    <c:if test="${user.authenticate.profileStatus != 'BLOCK'}">
                        <form style="margin-bottom: 0px;" action="../ChangeProfileStatusServlet" method="post">
                            <input type="hidden" name="lockUser" value="${bucketDebtors.user.id}"/>
                            <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="lockUser">Lock User</button>
                        </form>
                    </c:if>
                </li>
            </ul>
            <ul class="list-group">
                <li class="list-group-item">
                    <c:if test="${user.authenticate.profileStatus != 'TEMP_BLOCK'}">
                        <form style="margin-bottom: 0px;" action="../ChangeProfileStatusServlet" method="post">
                            <label for="blockedTimeUser">Days</label>
                            <input id="blockedTimeUser" name="blockedTimeUser" type="number" min="1" max="90" placeholder="blocked Days"/>
                            <input type="hidden" name="tempLockUser" value="${bucketDebtors.user.id}"/>
                            <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="tempLockUser">Temp Lock User</button>
                        </form>
                    </c:if>
                </li>
            </ul>
        </c:forEach>
    </div>
</div>
<div class="admin-panel">
    <h4 style="margin: 0;"><a href = "/IndexServlet">Main page.</a></h4>
</div>
</body>
</html>

