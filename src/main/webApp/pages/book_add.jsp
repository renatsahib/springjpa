<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library - Book Add</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/index.css" rel="stylesheet">
</head>
<body>
<h2>Book Add Form</h2>
<div class="container" style="width: 600px">
    <form action="../BookEditServlet" method="post" enctype="multipart/form-data">
        <div class="panel panel-default">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><label for="new_Title">Title:</label></td>
                        <td><input id="new_Title" name="new_Title" type="text" placeholder="Enter Title"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_Year">Year:</label></td>
                        <td><input id="new_Year" name="new_Year" type="number" min="0" max="2050" placeholder="Enter Year"/></td>
                    </tr>
                    <tr>
                        <td><label for="new_Count">Count</label></td>
                        <td><input id="new_Count" name="new_Count" type="number" min="0" max="100"  placeholder="Enter Count"/></td>
                    </tr>
                    <tr>

                        <td><label for="new_Genre">Genre</label></td>
                        <td>
                            <select id="new_Genre" name="new_Genre" size="1">
                                <c:set var="genreList" value="${requestScope.genreList}" />
                                <c:forEach var="genre" items="${genreList}" varStatus="i">
                                    <option value="${genre.id}">${genre.genre}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Authors:</label></td>
                        <td>
                            <select id="new_Authors1" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>

                            <select id="new_Authors2" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>

                            <select id="new_Authors3" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>

                            <select id="new_Authors4" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>

                            <select id="new_Authors5" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>

                            <select id="new_Authors6" name="new_Authors" size="1">
                                <option selected="selected" value="0"></option>
                                <c:set var="authorsList" value="${requestScope.authorsList}" />
                                <c:forEach var="authors" items="${authorsList}" varStatus="i">
                                    <option value="${authors.id}">${authors.name} ${authors.surname}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="new_Picture">Add picture</label></td>
                        <td><input id="new_Picture" accept="image/*" name="new_Picture" type="file"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <c:if test="${requestScope.status_addBook != null}">
            <div class="alert alert-danger" style="margin: 0">${requestScope.status_addBook}</div>
        </c:if>
        <button class="btn btn-default" type="submit" name="action" value="addBook">Add Book</button>
    </form>
</div>
</body>
</html>
