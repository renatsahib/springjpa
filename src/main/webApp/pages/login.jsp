<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <c:if test="${sessionScope.userSession == null}">
        <form class="form-signin" action="../RegistrationServlet" method="post">
            <h2 class="form-signin-heading">Please sign in</h2>
            <input class="form-control" id="login" name="login" type="text" placeholder="Enter Login"/>
            <input class="form-control" id="password" name="password" type="text" placeholder="Enter password"/>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="action" value="Login">Sign in</button>
        </form>
        <c:if test="${requestScope.error != null}">
            <div class="alert alert-danger">${requestScope.error}</div>
        </c:if>
        <h4><a href = "/pages/registration.jsp">Registration for new user.</a></h4>
    </c:if>


    <c:if test="${sessionScope.userSession != null}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-tittle">Hi <c:out value="${sessionScope.userSession.authenticate.login}"/>!</h3>
                </div>
            </div>
                <c:if test="${sessionScope.userSession.userRole == 'ADMIN'}">
                    <div class="panel panel-default">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Login</th>
                                    <th>Name</th>
                                    <th>Second Name</th>
                                    <th>Age</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:set var="userList" value="${sessionScope.userListSession}" />
                                <c:forEach var="user" items="${userList}" >
                                    <tr>
                                        <td>
                                            <c:out value="${user.authenticate.login}"></c:out>
                                        </td>
                                        <td><c:out value="${user.name}"></c:out></td>
                                        <td><c:out value="${user.surname}"></c:out></td>
                                        <td><c:out value="${user.age}"></c:out></td>
                                        <td><c:out value="${user.email}"></c:out></td>
                                        <td><c:if test="${user.userRole == 'ADMIN'}">admin</c:if>
                                            <c:if test="${user.userRole == 'USER'}">user</c:if>
                                        </td>
                                        <td>
                                            <form style="margin-bottom: 0px;" action="../RegistrationServlet" method="post">
                                                    <%--<c:set var="" scope="request" value="${}" />--%>
                                                <input type="hidden" name="loginDeleteUser" value="${user.authenticate.login}"/>
                                                <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="deleteUser">Delete User</button>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <form action="../RegistrationServlet" method="post">
                        <button class="btn btn-default" type="submit" name="action" value="addUser">Add User</button>
                    </form>
                </c:if>
        <form action="../LogOutServlet" method="post">
            <button class="btn btn-default" type="submit" name="logout" value="logout">Logout</button>
        </form>

    </c:if>

</div>

</body>
</html>
