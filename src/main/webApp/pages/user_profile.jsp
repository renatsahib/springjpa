<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library - user profile</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/signin.css" rel="stylesheet">
</head>
<body>
<div class="admin-panel">
    <c:set var="userList" value="${sessionScope.userListSession}" />
    <c:set var="userListBucket" value="${sessionScope.userListBucketSession}"/>
    <c:forEach var="user" items="${userList}" >
        <c:if test="${requestScope.userId == user.id}">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item">Login: <c:out value="${user.authenticate.login}"></c:out></li>
                    <li class="list-group-item">Status:
                    <c:if test="${user.authenticate.profileStatus == 'ACTIV'}">ACTIV</c:if>
                    <c:if test="${user.authenticate.profileStatus == 'TEMP_BlOCK'}">TEMP_BLOCK</c:if>
                    <c:if test="${user.authenticate.profileStatus == 'BLOCK'}">BLOCK</c:if>
                    <c:if test="${user.authenticate.profileStatus == 'TEMP_BlOCK'|| user.authenticate.profileStatus == 'BLOCK'}">
                        <form style="margin-bottom: 0px; padding-top: 10px;" action="../ChangeProfileStatusServlet" method="post">
                            <input type="hidden" name="unlockUser" value="${user.id}"/>
                            <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="unlockUser">Unlock User</button>
                        </form>
                    </c:if>
                    </li>
                    <c:if test="${user.blockedTime.blockedTime != null}">
                        <li class="list-group-item" style="color: red;">Blocked Time: <c:out value="${user.blockedTime.blockedTime}"></c:out></li>
                    </c:if>
                    <li class="list-group-item">Name: ${user.name}</li>
                    <li class="list-group-item">Second Name: ${user.surname}</li>
                    <li class="list-group-item">Email: ${user.email}</li>
                    <li class="list-group-item">Age: ${user.age}</li>
                    <li class="list-group-item">Role: ${user.userRole}</li>
                </ul>
            </div>
            <div class="panel panel-default">
                <h3 style="text-align: center">BUCKET</h3>
                <c:set var="userBucketList" value="${user.bucketSet}" />
                <c:forEach var="userBucket" items="${userBucketList}" >
                    <c:if test="${userBucket == null}">
                        <ul class="list-group">
                            <li class="list-group-item">Bucket empty</li>
                        </ul>
                    </c:if>
                    <c:if test="${userBucket != null}">
                        <ul class="list-group">
                            <li class="list-group-item">Title: <a href="/BookServlet?bookId=${userBucket.book.id}"><c:out value="${userBucket.book.tittle}"></c:out></a></li>
                            <li class="list-group-item">Reserved: <c:out value="${userBucket.reservedDays}"></c:out>
                                <form action="/BucketServlet" method="post" style="margin-bottom: 0px; padding-top: 10px;">
                                    <input type="hidden" id="bookId" name="bookId" value="${userBucket.book.id}"/>
                                    <button class="btn btn-default" type="submit" name="action" value="returnBook">Return Book</button>
                                </form>
                            </li>
                        </ul>
                    </c:if>
                </c:forEach>
            </div>
        </c:if>
    </c:forEach>

</div>


<div class="admin-panel">
    <h4 style="margin: 0;"><a href = "/IndexServlet">Main page.</a></h4>
</div>
</body>
</html>

