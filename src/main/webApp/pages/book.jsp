<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library - Book Description</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/index.css" rel="stylesheet">
</head>
<body>
<div class="admin-panel" style="padding: 10px">
    <%--     Set sessionScope.bookSession to book     --%>
    <c:set var="book" value="${sessionScope.bookSession}" />
    <div class="panel panel-default" style="width: 60%; margin: auto;">
        <H2 style="text-align: center">Book info</H2>
        <ul class="list-group">
            <li class="list-group-item">Title: <c:out value="${book.tittle}"></c:out></li>
            <li class="list-group-item">Year: <c:out value="${book.year}"></c:out></li>
            <li class="list-group-item">Count: <c:out value="${book.count}"></c:out></li>
            <li class="list-group-item">Genre: <c:out value="${book.bookGenre.genre}"></c:out></li>
            <c:set var="authors" value="${book.bookAuthorSet}" />
            <c:forEach var="author" items="${authors}" >
                <li class="list-group-item">Authors: <c:out value="${author.author.name}"></c:out> <c:out value="${author.author.surname}"></c:out></li>
            </c:forEach>
            <li class="list-group-item">Image: <img width="auto" height="180" src="/DisplayImageServlet?bookId=${sessionScope.bookSession.id}"></li>
        </ul>
    </div>
    <c:if test="${book.reserved == true}">
        <div class="panel panel-default" style="margin: auto; width: 80%">
            <p>All Book reserved!</p>
        </div>
    </c:if>
        <%--       Set sessionScope.userSession to user      --%>
    <c:set var="user" value="${sessionScope.userSession}" />
    <c:if test="${book.reserved != true}">
        <c:if test="${user.authenticate.profileStatus == 'ACTIV'}">
            <div class="panel panel-default" style="margin: 5px auto; width: 80%; padding: 10px 10px 0;">
                <ul class="list-group">
                    <c:if test="${user.authenticate.profileStatus == 'ACTIV'}">
                        <c:set var="bookInBucket" value="false"/>
                        <c:if test="${user.bucketSet != null}">
                            <c:set var="userBuckets" value="${user.bucketSet}" />
                            <c:forEach var="bucket" items="${userBuckets}" >
                                <c:if test="${bucket.book.id == book.id}">
                                    <c:set var="bookInBucket" value="true"/>
                                    <li class="list-group-item">This book in your bucket!</li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                        <c:if test="${bookInBucket == false}">
                            <li class="list-group-item">
                                <form action="../BucketServlet" method="post">
                                    <label for="reserved_days">Reserving days: </label>
                                    <input id="reserved_days" name="reserved_days" type="number" min="1" max="30" size="20" placeholder="1..30"/>
                                    <button class="btn btn-default" type="submit" name="action" value="AddToBucket">Add to Bucket</button>
                                </form>
                            </li>
                        </c:if>
                    </c:if>
                </ul>
            </div>
        </c:if>
    </c:if>

    <c:if test="${user.userRole == 'ADMIN'}">
        <div class="panel panel-default" style="margin: 5px auto; width: 80%; padding: 10px 10px 0;">
            <div>
                <form style="margin-bottom: 0px;" action="../BookEditServlet" method="post">
                    <input type="hidden" name="bookIdDeleteBook" value="${book.id}"/>
                    <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="deleteBook">Delete Book</button>
                </form>
            </div>
            <div>
                <form style="margin-bottom: 0px;" action="../BookEditServlet" method="post">
                    <input type="hidden" name="bookIdEditBook" value="${book.id}"/>
                    <button class="btn btn-default" style="padding: 0px 12px;" type="submit" name="action" value="goToEditBook">Edit Book</button>
                </form>
            </div>
        </div>
        <c:if test="${empty sessionScope.bucketByBookIdSession}">
            <div class="panel panel-default" style="width: 80%; margin: auto; padding: 5px 5px 0">
                <p>All Book in library.</p>
            </div>
        </c:if>
        <c:if test="${!empty sessionScope.bucketByBookIdSession}">
            <div class="panel panel-default" style="width: 80%; margin: auto; padding: 5px 5px 0">
                <c:set var="bucketByBookId" value="${sessionScope.bucketByBookIdSession}" />
                <c:forEach var="bucket" items="${bucketByBookId}" >
                    <c:set var="userListSession" value="${sessionScope.userListSession}" />
                    <c:forEach var="userList" items="${userListSession}" >
                        <c:if test="${bucket.user.id == userList.id}">
                            <p>Login: <a href="/UserProfileServlet?userId=${userList.id}"><c:out value="${userList.authenticate.login}"></c:out></a>  - reserved until <c:out value="${bucket.reservedDays}"></c:out></p>
                        </c:if>
                    </c:forEach>
                </c:forEach>
            </div>
        </c:if>
    </c:if>
    <div style="width: 60%; margin: auto; padding: 5px 5px 0">
        <h4 style="margin: 0;"><a href = "/IndexServlet">Main page.</a></h4>
    </div>
</div>
</body>
</html>
