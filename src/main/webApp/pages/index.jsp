<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library index page</title>
    <link href="../pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="../pages/css/index.css" rel="stylesheet">
</head>
<body>
    <div class="panel-title">
        <H1>Welcome to Online Library!</H1>
        <c:if test="${sessionScope.userSession == null}">
            <div class="authenticate-panel">
                <h4>Hi GUEST, please <a href="/pages/login.jsp">Authentication</a>  or <a href="/pages/registration.jsp">Registration</a> if you want to use all the functions of our Online Library.</h4>
            </div>
        </c:if>
        <c:if test="${sessionScope.userSession != null}">
            <h3>Hi <a href="/pages/user_panel.jsp"><c:out value="${sessionScope.userSession.authenticate.login}"/></a> !</h3>
        </c:if>
    </div>
    <div class="wrapper">
        <c:set var="bookList" value="${sessionScope.bookListSession}" />
        <c:forEach var="book" items="${bookList}" >
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Title: <c:out value="${book.tittle}"></c:out></p>
                    <p>Year: <c:out value="${book.year}"></c:out></p>
                    <p>Count: <c:out value="${book.count}"></c:out></p>
                    <p><a href="/BookServlet?bookId=${book.id}">Full description</a></p>
                </div>
            </div>
        </c:forEach>
    </div>
</body>
</html>
